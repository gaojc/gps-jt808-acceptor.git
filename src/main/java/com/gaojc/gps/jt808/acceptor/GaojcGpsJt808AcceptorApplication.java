package com.gaojc.gps.jt808.acceptor;

import com.zhoyq.server.jt808.starter.EnableJt808Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableJt808Server
public class GaojcGpsJt808AcceptorApplication {

    public static void main(String[] args) {
//        SpringApplication.run(GaojcGpsJt808AcceptorApplication.class, args);

        SpringApplication app = new SpringApplication(GaojcGpsJt808AcceptorApplication.class);
        // 不使用web容器 仅启动jt808服务
        app.setWebApplicationType(WebApplicationType.NONE);
        app.run(args);

    }

}
