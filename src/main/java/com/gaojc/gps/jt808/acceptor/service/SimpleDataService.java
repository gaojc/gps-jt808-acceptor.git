package com.gaojc.gps.jt808.acceptor.service;

import com.google.gson.Gson;
import com.zhoyq.server.jt808.starter.dto.SimAuthDto;
import com.zhoyq.server.jt808.starter.entity.*;
import com.zhoyq.server.jt808.starter.service.DataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Objects;

/**
 * jt808信号处理
 *
 * @author gaojc
 * @date 2021/06/30 17:46
 * @since v1.0
 */
@Service
@Slf4j
public class SimpleDataService implements DataService {
    public static final Gson gson = new Gson();

    @Override
    public void terminalRsa(String sim, byte[] e, byte[] n) {

    }

    @Override
    public byte[] terminalRsa(String sim) {
        return new byte[0];
    }

    @Override
    public void terminalAnswer(String sim, int platformStreamNumber, String platformCommandId, String msgId, byte[] msgBody) {
        log.info("终端通用应答【0001】,sim:{}, answer {},platformCommandId:{},msgId:{}", sim, msgId, platformCommandId, msgId);
    }

    @Override
    public void terminalHeartbeat(String sim) {
        log.info("心跳【0002】,sim:{}", sim);
    }

    @Override
    public void terminalCancel(String sim) {
        log.info("终端注销【0003】,sim:{}", sim);
    }

    @Override
    public String terminalRegister(String sim, int province, int city, String manufacturer, String deviceType, String deviceId, byte licenseColor, String registerLicense) {
        log.info("终端注册【0100】,sim:{},province:{},city:{},manufacturer:{}\n" +
                        "deviceType:{},deviceId:{},licenseColor:{},registerLicense:{}",
                sim, city, manufacturer, deviceType, deviceId, licenseColor, registerLicense);
        return registerLicense;
    }

    @Override
    public void terminalLocation(String sim, LocationInfo locationInfo, Integer mediaId) {
        log.info("信号位置数据【0200】sim:{},mediaId:{},location:{}", sim, mediaId, gson.toJson(locationInfo));
    }

    @Override
    public void eventReport(String sim, byte eventReportAnswerId) {
        log.info("事件报告【0000】sim:{},eventReportAnswerId:{}}", sim, eventReportAnswerId);
    }

    @Override
    public void orderInfo(String sim, byte type) {
        log.info("订单信息【00000】sim:{},type:{}", sim, type);
    }

    @Override
    public void cancelOrderInfo(String sim, byte type) {

    }

    @Override
    public void eBill(String sim, byte[] data) {
        try {
            log.info("电子运单【0701】sim:{},data:{}", sim, gson.toJson(new String(data, "utf-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void driverInfo(String sim, DriverInfo driverInfo) {
        log.info("驾驶员信息【0702】sim:{},driverInfo:{}", sim, gson.toJson(driverInfo));
    }

    @Override
    public void canData(String sim, CanDataInfo canDataInfo) {
        log.info("CAN总线数据上报【0705】sim:{},canDataInfo:{}", sim, gson.toJson(canDataInfo));
    }

    @Override
    public void mediaInfo(String sim, MediaInfo mediaInfo) {
        log.info("多媒体事件长传【0800】sim:{},mediaInfo:{}", sim, gson.toJson(mediaInfo));
    }

    @Override
    public void mediaPackage(String sim, byte[] mediaData, Integer mediaId) {
        String s = Objects.isNull(mediaData) ? "" : new String(mediaData);
        log.info("多媒体数据长传【0801】sim:{},mediaData:{}，mediaId:{}", sim, s, mediaId);
    }

    @Override
    public void dataTransport(String sim, DataTransportInfo dataTransportInfo) {
        log.info("数据上行透传【0900】,sim:{},dataTransportInfo:{}", dataTransportInfo);
    }

    @Override
    public void compressData(String sim, byte[] data) {
        String s = Objects.isNull(data) ? "" : new String(data);
        log.info("数据压缩上报【0901】sim:{},mediaInfo:{}", sim, gson.toJson(s));
    }

    @Override
    public void terminalAuth(String phone, String authId, String imei, String softVersion) {
        log.info("终端鉴权【0901】phone:{},authId:{},imei:{},softVersion:{}", phone,
                authId, imei, softVersion);
    }

    @Override
    public List<SimAuthDto> simAuth() {
        return null;
    }
}
