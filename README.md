# gps-jt808-acceptor (GPS-JT808协议数据接收器) #

###  说明: ###
交通部JT808国标协议的车载设备GPS信号点数据接收程序,解析16进制原始报文数据为可读对象,支持jt808各种命令数据的解析

### 作者 ###
1. email：jcgaogs@163.com
2. gitee：https://gitee.com/gaojc
3. github：https://github.com/gaojccn

### 版本: ###
#### V1.0.0 #### 
代码贡献者：gaojc
- 初版